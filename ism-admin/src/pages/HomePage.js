import Navbar from '../components/Navbar';
import Menu from '../components/Menu';
import PerfectScrollbar from 'react-perfect-scrollbar';

function HomePage() {
    return (
        <PerfectScrollbar>
            <div className="container-scroller">
                <Navbar></Navbar>
                <div className="container-fluid page-body-wrapper">
                    <div className="row row-offcanvas row-offcanvas-right">
                        <Menu></Menu>
                        <div className="content-wrapper">
                            <div className="row">
                                <div className="col-md-6 col-lg-3 grid-margin stretch-card">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-flex align-items-center justify-content-md-center">
                                                <i className="mdi mdi-basket icon-lg text-success"></i>
                                                <div className="ml-3">
                                                    <p className="mb-0">Daily Order</p>
                                                    <h6>12569</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-3 grid-margin stretch-card">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-flex align-items-center justify-content-md-center">
                                                <i className="mdi mdi-rocket icon-lg text-warning"></i>
                                                <div className="ml-3">
                                                    <p className="mb-0">Tasks Completed</p>
                                                    <h6>2346</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-3 grid-margin stretch-card">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-flex align-items-center justify-content-md-center">
                                                <i className="mdi mdi-diamond icon-lg text-info"></i>
                                                <div className="ml-3">
                                                    <p className="mb-0">Monthly Sales</p>
                                                    <h6>896546</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-3 grid-margin stretch-card">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-flex align-items-center justify-content-md-center">
                                                <i className="mdi mdi-chart-line-stacked icon-lg text-danger"></i>
                                                <div className="ml-3">
                                                    <p className="mb-0">Total Revenue</p>
                                                    <h6>$ 56000</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </PerfectScrollbar>
    );
}

export default HomePage;