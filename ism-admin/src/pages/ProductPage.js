import Navbar from '../components/Navbar';
import Menu from '../components/Menu';
import PerfectScrollbar from 'react-perfect-scrollbar';

function ProductPage() {
    return (
        <PerfectScrollbar>
            <div className="container-scroller">
                <Navbar></Navbar>
                <div className="container-fluid page-body-wrapper">
                    <div className="row row-offcanvas row-offcanvas-right">
                        <Menu></Menu>
                        <div className="content-wrapper">
                        </div>
                    </div>
                </div>
            </div>
        </PerfectScrollbar>
    );
}

export default ProductPage;