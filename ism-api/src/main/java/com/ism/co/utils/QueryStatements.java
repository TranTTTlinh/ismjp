/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:09:55 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.utils;

/**
 * The Class QueryStatements.
 */
public class QueryStatements {

	/** The Constant DETAIL_DATA_CATEGORY. */
	public static final String DETAIL_DATA_CATEGORY = ""
			+ "SELECT category_code,"
			+ "category_name,"
			+ "seo_url,"
			+ "image_url,"
			+ "delete_status,"
			+ "is_display,"
			+ "record_created_at,"
			+ "record_updated_at,"
			+ "record_created_by,"
			+ "record_updated_by "
			+ "FROM category "
			+ "WHERE category.category_code = ?"
			+ " AND category.delete_status=false";
	
	/** The Constant TOTAL_ITEM_ACTIVE_PAGING. */
	public static final String TOTAL_ITEM_CATEGORY_ACTIVE_PAGING = ""
			+ "SELECT COUNT(1) "
			+ "FROM category "
			+ "WHERE delete_status=false";
	
	/** The Constant CHECK_EXISTS_DATA_CATEGORY. */
	public static final String CHECK_EXISTS_DATA_CATEGORY = ""
			+ "SELECT COUNT(1) "
			+ "FROM category "
			+ "WHERE category_name = ? "
			+ "AND delete_status=false";
	
	/** The Constant GET_ALL_DATA_ACTIVE. */
	public static final String GET_ALL_DATA_ACTIVE = ""
			+ "SELECT "
			+ "category_code,"
			+ "category_name,"
			+ "seo_url,"
			+ "image_url,"
			+ "delete_status,"
			+ "is_display,"
			+ "record_created_at,"
			+ "record_updated_at,"
			+ "record_created_by,"
			+ "record_updated_by "
			+ "FROM category "
			+ "WHERE delete_status=false";

	/** The Constant DELETE_DATA_CATEGORY_LOGIC. */
	public static final String DELETE_DATA_CATEGORY_LOGIC = ""
			+ "UPDATE category SET "
			+ "delete_status = ?1,"
			+ "record_updated_at = CURRENT_TIMESTAMP "
			+ "WHERE category_code = ?2";
}
