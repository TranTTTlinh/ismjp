/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 8:32:42 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.dto;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The Class ResponseBody.
 */
@Component
@Data
public class ResponseBody {

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("error_code")
	private String errorCode;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("data")
	private Object data;
	
}
