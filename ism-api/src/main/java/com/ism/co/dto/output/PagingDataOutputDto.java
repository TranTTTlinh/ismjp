/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.dto.output;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * The Class PagingDataOutputDto.
 */
@Component

/**
 * Instantiates a new paging data output dto.
 */
@Data
public class PagingDataOutputDto<T> {
	
	/** The offset. */
	private int offset;
	
	/** The limit. */
	private int limit;
	
	/** The sort field. */
	private String sortField;
	
	/** The sort dir. */
	private String sortDir;
	
	/** The total page. */
	private int totalPage;

	/** The results. */
	private List<T> results = new LinkedList<T>();
	
}
