/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:11:14 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.dto.output.category;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CategoryOutputDTO.
 */
@Component
@Getter
@Setter
public class CategoryCreateOutputDto {
	
	private long categoryCode;
	
	private String categoryName;
	
	private String seoURL;
	
	private String imageURL;
	
	private String isDisplay;
	
	private Timestamp recordCreatedAt;
	
	private Timestamp recordUpdatedAt;
	
	private String recordCreatedBy;
	
	private String recordUpdatedBy;
	
	private boolean deleteStatus;
	
}
