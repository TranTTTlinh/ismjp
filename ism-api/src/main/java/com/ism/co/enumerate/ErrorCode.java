/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 8:32:42 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.enumerate;

/**
 * The Class ErrorCode.
 */
public enum ErrorCode {
	errorMissing("1000", "Input is missing field or not mapping data!", "ERROR"), 
	errorNullProperty("1001", "Data in field input is null!", "ERROR"),
	errorLongValue("1002", "Input value too long!", "ERROR"),
	errorNullValue("1003", "Input value is empty or input have data not found!", "ERROR"),
	errorWrongNumberPaging("1004", "Page size must not be less than one!", "ERROR"),
	errorExistsData("1005", "Data is exists on database!", "ERROR"),
	errorNotfound("1006", "", "ERROR");
    
	private String code;
	private String message;
	private String status;
	 
	ErrorCode(String code, String message, String status) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getStatus() {
		return status;
	}
	
}
