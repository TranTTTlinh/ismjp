/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:08:13 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ism.co.entities.Category;
import com.ism.co.utils.QueryStatements;

/**
 * The Interface ICategoryRepository.
 */
@Repository
public interface ICategoryRepository extends JpaRepository<Category, Long>  {
	
	/**
	 * Gets the data by primary key.
	 *
	 * @param categoryCode the category code
	 * @return the data by primary key
	 */
	@Query(value = QueryStatements.DETAIL_DATA_CATEGORY, nativeQuery = true)
	Category getDataByPrimaryKey(@Param("category_code") Long categoryCode);
    
    /**
     * Total item active paging.
     *
     * @return the int
     */
    @Query(value = QueryStatements.TOTAL_ITEM_CATEGORY_ACTIVE_PAGING, nativeQuery = true)
	int totalItemActivePaging();
    
    /**
     * Gets the all data category active.
     *
     * @param pageable the pageable
     * @return the all data category active
     */
    @Query(value = QueryStatements.GET_ALL_DATA_ACTIVE, nativeQuery = true)
    List<Category> getAllDataCategoryActive(Pageable pageable);
    
    /**
     * Gets the data category by ids.
     *
     * @param ids the ids
     * @return the data category by ids
     */
    @Query(value = QueryStatements.GET_ALL_DATA_ACTIVE, nativeQuery = true)
    List<Category> getDataCategoryByIds(@Param("ids") List<Long> ids);
    
	/**
	 * Check exists data category.
	 *
	 * @param categoryName the category name
	 * @return the category
	 */
	@Query(value = QueryStatements.CHECK_EXISTS_DATA_CATEGORY, nativeQuery = true)
	int checkExistsDataCategory(@Param("category_name") String categoryName);
	
	/**
	 * Delete data category logic.
	 *
	 * @param categorycode the categorycode
	 * @return the int
	 */
	@Modifying
	@Transactional
	@Query(value = QueryStatements.DELETE_DATA_CATEGORY_LOGIC, nativeQuery = true)
	int deleteDataCategoryLogic(@Param("delete_status") boolean deleteStatus, 
			@Param("category_code") long categoryCode);
}
