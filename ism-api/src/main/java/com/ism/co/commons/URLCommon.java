/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:14:39 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.commons;

/**
 * The Class URLCommon.
 */
public class URLCommon {

	/** The Constant URL_ROOT. */
	public static final String URL_ROOT = "/v1/api";
	
	/** The Constant URL_GROUP_PRODUCT. */
	public static final String URL_PRODUCT_GROUP = "/product-group";
	
	/** The Constant URL_CATEGORY_ON_PRODUCT_GROUP. */
	public static final String URL_CATEGORY_ON_PRODUCT_GROUP = "/category";
	
}
