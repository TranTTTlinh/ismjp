package com.ism.co;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.ism.co.configs.FileStorageProperties;

@SpringBootApplication
@EnableJpaAuditing
@EnableJpaRepositories(basePackages = {"com.ism.co.repositories"})
@EnableConfigurationProperties({ FileStorageProperties.class })
public class ImsMockJpCoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImsMockJpCoApplication.class, args);
	}
	
	/**
	 * Cors configurer.
	 *
	 * @return the web mvc configurer
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:8082");
			}
		};
	}

//	@Bean
//	public ModelMapper modelMapper() {
//	 return new ModelMapper();
//	}
	
}
