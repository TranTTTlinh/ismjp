/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 8:32:42 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ism.co.controllers.response.file.UploadFileResponse;
import com.ism.co.services.impl.FileStorageService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.nio.file.Path;
import java.util.stream.Collectors;

import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


@CrossOrigin
@RestController
@EnableAsync
public class FileController {
	
	private final String DIRECTORY = "D:\\02.Program\\02.Projects\\ismjp\\ism-api\\assets";

    /** The Constant logger. */
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    /** The file storage service. */
    @Autowired
    private FileStorageService fileStorageService;
    
    /**
     * Upload file.
     *
     * @param file the file
     * @return the upload file response
     */
    @PostMapping(value = "/v1/api/files/upload")
    @ResponseBody
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
    	String fileName = null;
    	String fileDownloadUri = null;
    	String fileShowUri = null;
        try {
        	fileName = fileStorageService.storeFile(file);

        	fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/v1/api/files/download/")
                    .path(fileName)
                    .toUriString();

        	fileShowUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/v1/api/files/")
                    .path(fileName)
                    .toUriString();
            Path fileStorage = get(DIRECTORY, fileName).toAbsolutePath().normalize();
			copy(file.getInputStream(), fileStorage, REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize(), fileShowUri);
    }

    /**
     * Upload multiple files.
     *
     * @param files the files
     * @return the list
     */
    @PostMapping("/v1/api/files/multi-upload")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    /**
     * Download file.
     *
     * @param fileName the file name
     * @param request the request
     * @return the response entity
     */
    @GetMapping("/v1/api/files/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
    /**
     * Gets the file.
     *
     * @param filename the filename
     * @return the file
     */
    @SuppressWarnings("rawtypes")
	@GetMapping("/v1/api/files/{filename:.+}")
	public ResponseEntity getFile(@PathVariable String filename) {
		Resource file = fileStorageService.loadFileAsResource(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
}
