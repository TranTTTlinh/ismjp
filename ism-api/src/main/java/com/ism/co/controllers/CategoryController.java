/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 8:32:42 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ism.co.commons.URLCommon;
import com.ism.co.controllers.request.PagingDataRequest;
import com.ism.co.controllers.request.category.CategoryCreateRequest;
import com.ism.co.controllers.request.category.CategoryDetailRequest;
import com.ism.co.controllers.response.PagingDataResponse;
import com.ism.co.controllers.response.category.CategoryCreateResponse;
import com.ism.co.controllers.response.category.CategoryDetailResponse;
import com.ism.co.controllers.response.category.CategoryListResponse;
import com.ism.co.converter.CategoryConverter;
import com.ism.co.dto.ResponseBody;
import com.ism.co.dto.input.PagingDataInputDto;
import com.ism.co.dto.input.category.CategoryDetailInputDto;
import com.ism.co.dto.output.PagingDataOutputDto;
import com.ism.co.dto.output.category.CategoryCreateOutputDto;
import com.ism.co.dto.output.category.CategoryDetailOutputDto;
import com.ism.co.dto.output.category.CategoryListOutputDto;
import com.ism.co.enumerate.ErrorCode;
import com.ism.co.enumerate.SuccessCode;
import com.ism.co.services.ICategoryService;

/**
 * The Class CategoryController.
 */
@CrossOrigin
@RestController
@EnableAsync
@RequestMapping(value = URLCommon.URL_ROOT + URLCommon.URL_PRODUCT_GROUP)
public class CategoryController {
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private ResponseBody response;
	
	@Autowired
	private CategoryConverter categoryConverter;

	/**
	 * Insert data category.
	 *
	 * @param categoryCreateRequest the category create request
	 * @return the response entity
	 */
	@PostMapping(URLCommon.URL_CATEGORY_ON_PRODUCT_GROUP + "/create")
	public ResponseEntity<ResponseBody> createDataCategory(
			final @RequestBody CategoryCreateRequest categoryCreateRequest) {
		CategoryCreateOutputDto categoryCreateOutputDto = null;
		CategoryCreateResponse categoryCreateResponse = null;
		try {
			// Check too long data
			if (categoryCreateRequest.getCategoryName().length() >= 255) {
				response.setMessage(ErrorCode.errorLongValue.getMessage());
				response.setErrorCode(ErrorCode.errorLongValue.getCode());
				response.setStatus(ErrorCode.errorLongValue.getStatus());
				response.setData("");
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			
			// Check null data
			if (categoryCreateRequest.getCategoryName().length() <= 0) {
				response.setMessage(ErrorCode.errorNullValue.getMessage());
				response.setErrorCode(ErrorCode.errorNullValue.getCode());
				response.setStatus(ErrorCode.errorNullValue.getStatus());
				response.setData("");
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			
			// Check exists data
			if (categoryService.checkExistsDataCategory(categoryCreateRequest.getCategoryName()) >= 1) {
				response.setMessage(ErrorCode.errorExistsData.getMessage());
				response.setErrorCode(ErrorCode.errorExistsData.getCode());
				response.setStatus(ErrorCode.errorExistsData.getStatus());
				response.setData("");
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			
			// Insert and get data category from service then return output data
			categoryCreateOutputDto = categoryService.insertDataCategory(
					categoryConverter.convertRequestBodyToInputDtoOnControllerToCreateData(
							categoryCreateRequest));
			
			// Return data to response body when inserted data success
			categoryCreateResponse = categoryConverter.
					convertOutputDtoToResponseBodyOnControllerToCreateDataWhenCreate(
							categoryCreateOutputDto);
			
			// Add data on response body defined in 「com.ism.co.dto」 and 「com.ism.co.enumerate」
			response.setMessage(SuccessCode.insertSuccess.getMessage());
			response.setErrorCode(SuccessCode.insertSuccess.getCode());
			response.setStatus(SuccessCode.insertSuccess.getStatus());
			response.setData(categoryCreateResponse);
			
			return new ResponseEntity<ResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			// Catch exception with message and add on 「com.ism.co.dto」 and 「com.ism.co.enumerate」
			if (e.getMessage().contains("At least") || 
					e.getMessage().contains("parameter(s) provided but only")) {
				response.setMessage(ErrorCode.errorMissing.getMessage());
				response.setErrorCode(ErrorCode.errorMissing.getCode());
				response.setStatus(ErrorCode.errorMissing.getStatus());
				response.setData(e.getMessage());
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			} else if (e.getMessage().contains("not-null property references a null")) {
				response.setMessage(ErrorCode.errorNullProperty.getMessage());
				response.setErrorCode(ErrorCode.errorNullProperty.getCode());
				response.setStatus(ErrorCode.errorNullProperty.getStatus());
				response.setData(e.getMessage());
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			response.setMessage(ErrorCode.errorNotfound.getMessage());
			response.setErrorCode(ErrorCode.errorNotfound.getCode());
			response.setStatus(ErrorCode.errorNotfound.getStatus());
			response.setData(e.getMessage());
			
			return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Gets the detail data category.
	 *
	 * @param categoryCreateRequest the category create request
	 * @return the detail data category
	 */
	@GetMapping(URLCommon.URL_CATEGORY_ON_PRODUCT_GROUP + "/get")
	public ResponseEntity<ResponseBody> getDetailDataCategory(
			final @RequestBody CategoryDetailRequest categoryDetailRequest) {
		CategoryDetailOutputDto categoryDetailOutputDto = null;
		CategoryDetailResponse categoryDetailResponse = null;
		try {
			// Convert detail request to input dto
			CategoryDetailInputDto categoryDetailInputDto = categoryConverter.
					convertRequestBodyToInputDtoOnControllerToGetDataByCode(
							categoryDetailRequest);
			
			// Check null data
			if (categoryDetailRequest.getCategoryCode() == 0) {
				response.setMessage(ErrorCode.errorNullValue.getMessage());
				response.setErrorCode(ErrorCode.errorNullValue.getCode());
				response.setStatus(ErrorCode.errorNullValue.getStatus());
				response.setData("Id of category not found!");
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			
			// Call service to get data category by code
			categoryDetailOutputDto = categoryService.getDataCategoryByPrimaryKey(categoryDetailInputDto);
			
			// Data not found to show id wrong
			if (categoryDetailOutputDto == null) {
				response.setMessage(ErrorCode.errorNullValue.getMessage());
				response.setErrorCode(ErrorCode.errorNullValue.getCode());
				response.setStatus(ErrorCode.errorNullValue.getStatus());
				response.setData("Id of category not found!");
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			
			// Convert from output to response body
			categoryDetailResponse = categoryConverter.
					convertOutputDtoToResponseBodyOnControllerToGetDataByCode(
							categoryDetailOutputDto);
			
			// Add data on response body defined in 「com.ism.co.dto」 and 「com.ism.co.enumerate」
			response.setMessage(SuccessCode.insertSuccess.getMessage());
			response.setErrorCode(SuccessCode.insertSuccess.getCode());
			response.setStatus(SuccessCode.insertSuccess.getStatus());
			response.setData(categoryDetailResponse);
			
			return new ResponseEntity<ResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			if (e.getMessage().contains("Cannot invoke") || 
					e.getMessage().contains("is null")) {
				response.setMessage(ErrorCode.errorNullValue.getMessage());
				response.setErrorCode(ErrorCode.errorNullValue.getCode());
				response.setStatus(ErrorCode.errorNullValue.getStatus());
				response.setData(e.getMessage());
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			response.setMessage(ErrorCode.errorNotfound.getMessage());
			response.setErrorCode(ErrorCode.errorNotfound.getCode());
			response.setStatus(ErrorCode.errorNotfound.getStatus());
			response.setData(e.getMessage());
//			 e.printStackTrace();
			return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Find all data paging.
	 *
	 * @param model the model
	 * @return the response entity
	 */
	@PostMapping(URLCommon.URL_CATEGORY_ON_PRODUCT_GROUP + "/list")
	public ResponseEntity<ResponseBody> findAllDataPaging(
			final @RequestBody PagingDataRequest model) {
		PagingDataOutputDto<CategoryListOutputDto> pagingDataOutputDto = null;
		PagingDataResponse<CategoryListResponse> responsePaging = null;
		List<CategoryListResponse> categoryListResponses = new LinkedList<CategoryListResponse>();
		try {
			// Convert from request paging to input paging to put on function service
			PagingDataInputDto pagingDataInputDto = categoryConverter.
					convertRequestBodyToInputDtoOnControllerToHandlePaging(model);
			
			// Get data paging category from service
			pagingDataOutputDto = categoryService.
					findAllDataWithPaging(pagingDataInputDto);
			
			for (CategoryListOutputDto item : pagingDataOutputDto.getResults()) {
				CategoryListResponse categoryListResponse = categoryConverter.
						convertOutputDtoToResponseBodyOnControllerToGetListResultCategoryPaging(
								item);
				categoryListResponses.add(categoryListResponse);
			}
			
			// Convert paging category output dto to paging category response body
			responsePaging = categoryConverter.
					convertOuputDtoToResponseBodyOnControllerToGetDataPaging(
							pagingDataOutputDto);
			responsePaging.setResults(categoryListResponses);
			
			// Add data on response body defined in 「com.ism.co.dto」 and 「com.ism.co.enumerate」
			response.setMessage(SuccessCode.getDataSuccess.getMessage());
			response.setErrorCode(SuccessCode.getDataSuccess.getCode());
			response.setStatus(SuccessCode.getDataSuccess.getStatus());
			response.setData(responsePaging);
			
			return new ResponseEntity<ResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			if (e.getMessage().contains("Page size must not be less") || 
					e.getMessage().contains("than one")) {
				response.setMessage(ErrorCode.errorNullValue.getMessage());
				response.setErrorCode(ErrorCode.errorNullValue.getCode());
				response.setStatus(ErrorCode.errorNullValue.getStatus());
				response.setData(e.getMessage());
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			if (e.getMessage().contains("is null")) {
				response.setMessage(ErrorCode.errorNullValue.getMessage());
				response.setErrorCode(ErrorCode.errorNullValue.getCode());
				response.setStatus(ErrorCode.errorNullValue.getStatus());
				response.setData(e.getMessage());
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			if (e.getMessage().contains("Page index must not be less than zero")) {
				response.setMessage(ErrorCode.errorWrongNumberPaging.getMessage());
				response.setErrorCode(ErrorCode.errorWrongNumberPaging.getCode());
				response.setStatus(ErrorCode.errorWrongNumberPaging.getStatus());
				response.setData(e.getMessage());
				return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
			}
			response.setMessage(ErrorCode.errorNotfound.getMessage());
			response.setErrorCode(ErrorCode.errorNotfound.getCode());
			response.setStatus(ErrorCode.errorNotfound.getStatus());
			response.setData(e.getMessage());
//			e.printStackTrace();
			return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Delete data category logic.
	 *
	 * @param categoryCode the category code
	 * @return the response entity
	 */
	@DeleteMapping(URLCommon.URL_CATEGORY_ON_PRODUCT_GROUP + "/delete/{category_code}")
	public ResponseEntity<ResponseBody> deleteDataCategoryLogic(
			@PathVariable("category_code") long categoryCode) {
		try {
			int rowsUpdate = categoryService.deleteDataCategoryLogic(true, categoryCode);

			if (rowsUpdate > 0 ) {
				response.setMessage(SuccessCode.deleteDataSuccess.getMessage());
				response.setErrorCode(SuccessCode.deleteDataSuccess.getCode());
				response.setStatus(SuccessCode.deleteDataSuccess.getStatus());
				response.setData(rowsUpdate);
				
				return new ResponseEntity<ResponseBody>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			response.setMessage(ErrorCode.errorNotfound.getMessage());
			response.setErrorCode(ErrorCode.errorNotfound.getCode());
			response.setStatus(ErrorCode.errorNotfound.getStatus());
			response.setData(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Delete data more category logic.
	 *
	 * @param categoryCode the category code
	 * @return the response entity
	 */
	@DeleteMapping(URLCommon.URL_CATEGORY_ON_PRODUCT_GROUP + "/deletes")
	public ResponseEntity<ResponseBody> deleteDataMoreCategoryLogic(
			@RequestBody long[] categoryCode) {
		try {
			categoryService.deleteMoreDataCategoryLogic(true, categoryCode);
			
			response.setMessage(SuccessCode.deleteDataSuccess.getMessage());
			response.setErrorCode(SuccessCode.deleteDataSuccess.getCode());
			response.setStatus(SuccessCode.deleteDataSuccess.getStatus());
			response.setData("");
			
			return new ResponseEntity<ResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			response.setMessage(ErrorCode.errorNotfound.getMessage());
			response.setErrorCode(ErrorCode.errorNotfound.getCode());
			response.setStatus(ErrorCode.errorNotfound.getStatus());
			response.setData(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseBody>(response, HttpStatus.BAD_REQUEST);
		}
	}
}
