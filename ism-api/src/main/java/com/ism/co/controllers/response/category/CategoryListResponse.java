/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers.response.category;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CategoryListResponse.
 */
@Component
@Getter
@Setter
public class CategoryListResponse {
	
	@JsonProperty("category_code")
	private long categoryCode;
	
	@JsonProperty("category_name")
	private String categoryName;
	
	@JsonProperty("seo_url")
	private String seoURL;
	
	@JsonProperty("image_url")
	private String imageURL;
	
	@JsonProperty("delete_status")
	private boolean deleteStatus;
	
	@JsonProperty("is_display")
	private String isDisplay;
	
	@JsonProperty("record_created_at")
	private Timestamp recordCreatedAt;
	
	@JsonProperty("record_updated_at")
	private Timestamp recordUpdatedAt;
	
	@JsonProperty("record_created_by")
	private String recordCreatedBy;
	
	@JsonProperty("record_updated_by")
	private String recordUpdatedBy;
	
}
