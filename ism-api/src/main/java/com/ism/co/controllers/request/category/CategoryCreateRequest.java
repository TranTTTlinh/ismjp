/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:10:13 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers.request.category;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The Class CreateCategoryRequest.
 */
@Component
@Data
public class CategoryCreateRequest {

	@JsonProperty("category_name")
	private String categoryName;
	
	@JsonProperty("seo_url")
	private String seoURL;
	
	@JsonProperty("image_url")
	private String imageURL;

	@JsonProperty("record_created_by")
	private String recordCreatedBy;
	
	@JsonProperty("record_updated_by")
	private String recordUpdatedBy;
}
