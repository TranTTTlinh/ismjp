/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers.response;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The Class PagingDataResponse.
 */
@Component
@Data
public class PagingDataResponse<T> {
	
	/** The offset. */
	@JsonProperty("offset")
	private int offset;
	
	/** The limit. */
	@JsonProperty("limit")
	private int limit;
	
	/** The sort field. */
	@JsonProperty("sort_field")
	private String sortField;
	
	/** The sort dir. */
	@JsonProperty("sort_dir")
	private String sortDir;
	
	/** The total page. */
	@JsonProperty("total_page")
	private int totalPage;

	/** The results. */
	@JsonProperty("results")
	private List<T> results = new LinkedList<T>();

}
