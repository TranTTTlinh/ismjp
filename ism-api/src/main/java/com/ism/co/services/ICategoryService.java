/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:10:13 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.services;

import org.springframework.stereotype.Service;

import com.ism.co.dto.input.PagingDataInputDto;
import com.ism.co.dto.input.category.CategoryCreateInputDto;
import com.ism.co.dto.input.category.CategoryDetailInputDto;
import com.ism.co.dto.output.PagingDataOutputDto;
import com.ism.co.dto.output.category.CategoryCreateOutputDto;
import com.ism.co.dto.output.category.CategoryDetailOutputDto;
import com.ism.co.dto.output.category.CategoryListOutputDto;

/**
 * The Interface ICategoryService.
 */
@Service
public interface ICategoryService {

	/**
	 * Insert data category.
	 *
	 * @param object the object
	 * @return the category create input dto
	 */
	CategoryCreateOutputDto insertDataCategory(CategoryCreateInputDto object);
	
	/**
	 * Gets the data category by primary key.
	 *
	 * @param object the object
	 * @return the data category by primary key
	 */
	CategoryDetailOutputDto getDataCategoryByPrimaryKey(CategoryDetailInputDto object);
	
	/**
	 * Total item.
	 *
	 * @return the int
	 */
	int totalItem();
	
	/**
	 * Find all data with paging.
	 *
	 * @param model the model
	 * @return the list
	 */
	PagingDataOutputDto<CategoryListOutputDto> findAllDataWithPaging(PagingDataInputDto model);
	
	/**
	 * Check exists data category.
	 *
	 * @param categoryName the category name
	 * @return the int
	 */
	int checkExistsDataCategory(String categoryName);
	
	/**
	 * Delete data category logic.
	 *
	 * @param categoryCode the category code
	 * @return the int
	 */
	int deleteDataCategoryLogic(boolean deleteStatus, long categoryCode);
	
	
	/**
	 * Delete more data category logic.
	 *
	 * @param deleteStatus the delete status
	 * @param categoryCode the category code
	 * @return the int
	 */
	void deleteMoreDataCategoryLogic(boolean deleteStatus, long[] categoryCodes);
}
