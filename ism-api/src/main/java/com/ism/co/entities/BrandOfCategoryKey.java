/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Mar 12, 2022 10:15:16 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The Class BrandOfCategoryKey.
 */
@Embeddable
public class BrandOfCategoryKey implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "category_code")
    private long categoryCode;

    @Column(name = "brand_code")
    private long brandCode;
}
