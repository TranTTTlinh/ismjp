/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Category.
 */
@Entity(name = "category")
@Table(name = "category")
@Getter
@Setter
public class Category extends BaseEntity {

	@ManyToMany
    @JoinTable(name = "brand_of_category", 
    	joinColumns = @JoinColumn(name = "category_code"), 
    	inverseJoinColumns = @JoinColumn(name = "brand_code"))
    private List<Brand> listBrandOfCategory;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long categoryCode;
	
	@Column(name = "category_name")
	private String categoryName;
	
	@Column(name = "seo_url")
	private String seoURL;
	
	@Column(name = "image_url")
	private String imageURL;
	
	@Column(name = "is_display")
	private String isDisplay;
	
	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
	private List<SubCategory> listSubCategoryAndCategory;
	
	@OneToMany(mappedBy = "categoryProduct", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Product> productListOnCategory;
}
