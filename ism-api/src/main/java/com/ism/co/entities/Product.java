/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:52:51 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.ForeignKey;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Product.
 */
@Entity(name = "product")
@Table(name = "product")
@Getter
@Setter
public class Product extends BaseEntity {
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_detail_code")
    private ProductDetail productDetail;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long productCode;
	
	@Column(name = "product_name")
	private String productName;
	
	@Column(name = "old_price")
	private int oldPrice;
	
	@Column(name = "seo_url")
	private String seoURL;
	
	@Column(name = "image_url")
	private String imageURL;
	
	@Column(name = "new_price")
	private int newPrice;
	
	@Column(name = "from_price")
	private int fromPrice;
	
	@Column(name = "to_price")
	private int toPrice;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "image_code")
	private long imageCode;
	
	@Column(name = "category_code")
	private long categoryCode;
	
	@Column(name = "brand_code")
	private long brandCode;
	
	@OneToMany(mappedBy = "productImg", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
	private List<Image> listImageAndProduct;
	
	@ManyToOne()
	@JoinColumn(name = "category_code", insertable = false, updatable = false, 
			foreignKey = @ForeignKey(name = "fk_product_category"))
	@JsonIgnore
	private Category categoryProduct;

	
	@ManyToOne()
	@JoinColumn(name = "brand_code", insertable = false, updatable = false, 
			foreignKey = @ForeignKey(name = "fk_product_brand"))
	@JsonIgnore
	private Brand brandProduct;

}
