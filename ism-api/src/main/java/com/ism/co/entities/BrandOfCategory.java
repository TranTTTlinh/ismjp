/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:42 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class BrandOfCategory.
 */
@Entity(name = "brand_of_category")
@Table(name = "brand_of_category")
@Getter
@Setter
public class BrandOfCategory {
	
	/**
	 * Like as a table act as an intermediary then use 
	 * @EmbeddedId to mark key pair as primary key 
	 * (PRIMARY KEY ) on entity.
	 */
	@EmbeddedId
	private BrandOfCategoryKey brandOfCategoryKey;
	
	@ManyToOne
    @MapsId("category_code")
    @JoinColumn(name = "category_code")
    private Category category;

    @ManyToOne
    @MapsId("brand_code")
    @JoinColumn(name = "brand_code")
    private Brand brand;
}
