/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:36 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Brand.
 */
@Entity(name = "brand")
@Table(name = "brand")
@Getter
@Setter
public class Brand extends BaseEntity {

	@ManyToMany(mappedBy = "listBrandOfCategory")
    private List<Category> categoryOfBrand;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long brandCode;
	
	@Column(name = "brand_name")
	private String brandName;
	
	@Column(name = "seo_url")
	private String seoURL;
	
	@Column(name = "image_url")
	private String imageURL;
	
	@OneToMany(mappedBy = "brandProduct", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Product> productListOnBrand;

}
