/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:11 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * The Class BaseEntity.
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
		value = {
				"record_created_at",
				"record_updated_at",
				"record_created_by",
				"record_updated_by", 
				"delete_status"
		}, 
		allowGetters = true, 
		allowSetters = true)
@Data
public class BaseEntity {

	@Column(name = "record_created_at", nullable = true, updatable = false)
	@CreatedDate
	private Timestamp recordCreatedAt;
	
	@Column(name = "record_updated_at", nullable = true, updatable = false)
	@LastModifiedDate
	private Timestamp recordUpdatedAt;
	
	@Column(name = "record_created_by", nullable = false, updatable = false)
	@CreatedBy
	private String recordCreatedBy;
	
	@Column(name = "record_updated_by", nullable = false, updatable = false)
	@LastModifiedBy
	private String recordUpdatedBy;
	
	@Column(name = "delete_status", nullable = false, updatable = false)
	private boolean deleteStatus;
	
}
