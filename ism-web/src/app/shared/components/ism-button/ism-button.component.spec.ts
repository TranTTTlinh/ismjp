import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IsmButtonComponent } from './ism-button.component';

describe('IsmButtonComponent', () => {
  let component: IsmButtonComponent;
  let fixture: ComponentFixture<IsmButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsmButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsmButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
