import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IsmButtonComponent } from './shared/components/ism-button/ism-button.component';
import { IsmGridComponent } from './shared/components/ism-grid/ism-grid.component';
import { IsmModalComponent } from './shared/components/ism-modal/ism-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    IsmButtonComponent,
    IsmGridComponent,
    IsmModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
