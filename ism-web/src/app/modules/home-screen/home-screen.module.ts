import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    ContentComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentComponent,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./content/content.module').then((m) => m.ContentModule),
          },
        ],
      },
    ])
  ]
})
export class HomeScreenModule { }
